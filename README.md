# Just an easy Hex printer, didn't want to calculate it myself
[![build status](https://gitlab.com/Mats/hexprinter/badges/master/build.svg)](https://gitlab.com/Mats/hexprinter/commits/master)

## Installation
```
    Clone this repo

    mvn clean package

    Enjoy the jar file in the target directory

```

## Direct download link
[Downloads](https://gitlab.com/Mats/hexprinter/tags/)

## Usage
```
    java -jar HexPrinter.jar (integers)

    Example:

    java -jar HexPrinter.jar 45 12 64